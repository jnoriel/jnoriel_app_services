set -o nounset

#!/bin/bash

test_existance() {
	FILE=$1    

	if [ -e $FILE ]; then
	   echo "File: $FILE exists."

	else
	   echo "File: $FILE cannot be found."

	fi
}