#! /bin/bash

#Stores the first parameter into the variable file_name
file_name=$1

#Stores the output of test into the variable testOutput
testOutput=$( test -e $file_name )

#Stores the exit status of test into output
output=$?

#Check if output is a 0, prints exists, else prints can't be found
if [[ "$output" == 0 ]]; then
	echo "The filename: $file_name exists"
else
	echo "The filename: $file_name can't be found"
fi

#Exit with the exit code of the output
exit $output
