#Setup mail.s12.as.learn

#Install dovecot package
echo "Installing and Enabling dovecot"
yum install -y dovecot 
systemctl enable dovecot.service

#Configure dovecot.cf 
echo "Configuring dovecot.conf"
cp ./dovecot.conf/etc/dovecot/dovecot.conf

# /etc/dovecot/conf.d/10-mail.conf
echo "Configuring 10-mail.conf"
cp ./10-mail.conf /etc/dovecot/conf.d/10-mail.conf
# /etc/dovecot/conf.d/10-auth.conf
echo "Configuring 10-auth.conf"
cp ./10-auth.conf /etc/dovecot/conf.d/10-auth.conf

echo "Configuring 10-master.conf"
cp ./10-master.conf /etc/dovecot/conf/10-master.conf

#restart dovecot
echo "Restarting dovecot"
systemctl restart dovecot 