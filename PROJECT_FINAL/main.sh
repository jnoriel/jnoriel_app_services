#! /bin/bash

#Main.sh = Main invocation script. Calling other scripts

. ./base_configuration.sh
. ./selinux_setup.sh
. ./network_script.sh
. ./net_if_setup.sh

FILE=main.conf
vars=()
declare -A options

while read LINE; do
        vars+=($LINE)
done < $FILE
for var in ${vars[@]}; do
        splitVars=(${var//=/ })
        options[${splitVars[0]}]=${splitVars[1]}
done


#To deploy WiFi configuration, set wifi=true

if [[ ${options["hostapd"]} == "true" ]]; then
	echo "Configuring WiFi with WPA2-Key - SSID: NASP19_12"
	. ./wifi_if_setup.sh ${options["wifiInt"]} ${options["wifiSSID"]}
else
	echo "Ignoring WiFi setup"

fi

#To deploy OSPF configuration, set OSPF=true
if [[ ${options["ospf"]} == "true" ]]; then
	echo "Configuring OSPF/Routing setup"
	. ./ospf_setup.sh
else 
	echo "Ignoring OSPF/Routing setup"

fi

#To deploy DHCP configuration, set DHCP=true

if [[ ${options["dhcp"]} == "true" ]]; then
	echo "Configuring DHCP setup"
	. ./dhcpd_setup.sh #${options["dhcpInts"]}
else
	echo "Ignoring DHCP setup"

fi

#To deploy NSD configuration, set NSD=true

if [[ ${options["nsd"]} == "true" ]]; then
	echo "Confugiring NSD setup"
	. ./nsd_setup.sh
else 
	echo "Ignoring NSD setup"

fi

#To deploy unbound configuration, set unbound=true
if [[ ${options["unbound"]} == "true" ]]; then
	echo "Configuring unbound setup"
	. ./unbound_setup.sh
else 
	echo "Ignoring unbound setup"

fi

#To set basic iptables confuguration, set iptables=true

if [[ ${options["iptables"]} == "true" ]]; then
	echo "Configuring iptables setup"
	. ./iptables_setup.sh
else
	echo "Ignoring iptables setup"

fi 