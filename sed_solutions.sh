#!/bin/bash
#Task1: Emulate grep with sed. Print all lines starting with option.
#sed -n '/option/p' dhcpd.conf # -n forces the lines to only print out once with /p ending

#Task2: Delete all blank lines in the file
#sed -n -i '/[:blank:]/p' dhcpd.conf # iunno -i is saving the printed result

#Task3: Add the following line to the beginning of the file: # NASP19 sed DHCP Configuration
#sed -i '1i\\# NASP19 sed DHCP Configuration' dhcpd.conf

#Task4: Remove all comments at the end of line (hints : (1) what does ^ match? (2) what does [^] match? (3) what does [^^] match?
#sed -r -i 's/^([^^#]*).#.*/\1/' dhcpd.conf #Takes any lines that don't start with # and contains a #, stores the line from start to the # and removes the part from # til end

#Task5: Change the router IP address to 10.10.0.1. Note that there is more than one empty space between "option" and "routers". Your expression should therefore match multiple empty spaces. To match any white space, use [[:space:]]. This will match tab, space, etc.
#sed -r -i 's/(option.[[:space:]].*routers.[[:space:]]+).*[0-9]/\110.10.0.1/' dhcpd.conf

#Task6: Report the range of IP addresses. The output should be in the form: IP range is : 10-100. I.e., do not report the rest of IP addresses.
#sed -r -n 's/range.[0-9]+.[0-9]+.[0-9]+.([0-9]+).[0-9]+.[0-9]+.[0-9]+.([0-9]+).*/IP range is: \1 to \2/p' dhcpd.conf

#Task7: Prompt the user to change the address range (last octet in the /24 network). Your script shall accept a range, check the range for validity, and update the file. Do not assume that you know the network part of the range. Only the range itself should be updated by sed.
range1=$1
range2=$2

#TODO: 
#Check if there are two parameters
#Check if range1 and range2 are numbers
#Check if range1 > range2

sed -i -e "/.*range/ s/\.[0-9]*[ ;]/\.$range1 /g1" -e "/.*range/ s/\.[0-9]*[ ;]/\.$range2 /g2" dhcpd.conf