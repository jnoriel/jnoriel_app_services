#! /bin/bash

#Configuring dhcpd service

echo "Installing dhcpd service"
yum install -y dhcp

echo "Overriding installed package unit file"
cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/dhcpd.service 

echo "Configuring dhcpd.conf"
cp ./dhcpd.conf /etc/dhcp/dhcpd.conf

echo "Reloading daemon"
systemctl daemon-reload

echo "Starting and Enabling dhcpd"
systemctl start dhcpd
systemctl enable dhcpd