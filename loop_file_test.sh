#!/bin/bash

#Takes all arguments and puts them into a list
files_to_test="$@"

#Loads functions from logic file and executes all code not in a function
source ./loop_file_logic.sh 

#Loops through the list of files and calls a function from the sourced file
#file can be any name, just make it so you know what the variable is storing
#so its easier to understand
for file in $files_to_test; do
	test_existance $file
done
