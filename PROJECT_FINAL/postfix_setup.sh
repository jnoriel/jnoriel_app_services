#! /bin/bash

#Configure PostFix or Mail Transfer Agent (MTA)

#Synchronizing the system clock to Vancouver/Local Time or remote Server
echo "Synchronizing timezone to America/Vancouver"
timedatectl set-timezone America/Vancouver
timedatectl set-ntp yes 

#Install Telnet - Testing Tool 
echo "Installing Telnet"
yum install -y telnet

#Configure Postfix to listen to all interfaces
echo "Configuring Postfix" 
cp ./main.cf /etc/postfix/main.cf 
cp ./master.cf /etc/postfix/master.cf

#Configure SASL and SSL
#echo "Configuring SSL and SASL"
#cp ./mail.s12.as.learn.key /etc/pki/tls/certs/
#cp ./mail.s12.as.learn.0.csr /etc/pki/tls/certs/
#cp ./mail.s12.as.learn.crt /etc/pki/tls/private/

#Restart postfix daemon
echo "Restarting Postfix Daemon"
systemctl restart postfix.service
