#! /bin/bash

#Configure and install wireless interface settings

#Install hostapd
yum install -y hostapd

#Copy ifcfg-wlp0s6u1
echo "Configuring ifcfg-wlp0s6u2 (Wireless)"
cp ifcfg-wlp0s6u2 /etc/sysconfig/network-scripts

#Copy /etc/hostapd configuration file
echo "Configurting hostapd.conf"
cp hostapd.conf /etc/hostapd/hostapd.conf

#Restart network.service
echo "Restarting network.service"
systemctl restart network.service

#Start and Enable hostapd
echo "Starting and Enabling hostapd"
systemctl start hostapd
systemctl enable hostapd 