#! /bin/bash

#base_configuration.sh 
#Configure basic system for Centos7

set -o nounset                              # Treat unset variables as an error

#Install base packages

echo "Installing base packages"
yum -y update
echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install base

#Install epel-release

echo "Installing the Extra Packages for Enterprise Linux Repository" 
yum -y install epel-release
yum -y update

#Install nmap-ncat, tcpdump, nmap and git

echo "Installing project specific tools"
yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git

#Install VB Gues Additions

echo "Setting Up VirtualBox Guest Additions"
echo "Installing pre-requisities"
yum -y install kernel-devel kernel-headers dkms gcc gcc-c+

#Mount and Install Guest Additions

echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
mkdir vbox_cd
mount /dev/cdrom ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd

#Set up hostname
echo "Changing hostname to mail.s12.as.learn:"
hostnamectl set-hostname mail.s12.as.learn

#See if there are additional ones to be added! 