#! /bin/bash

#Configure DNS (authoritative name service)
echo "Installing and configuring NSD"
yum install -y nsd

#Copy (reverse zone file)
echo "Creating reverse-look up zone file for NSD"
cp 12.16.10.in-addr.arpa.zone /etc/nsd

#Copy (DNS lookup file)
echo "Creating DNS look-up zone file for NSD"
cp s12.as.learn.zone /etc/nsd

#Edit and configure nsd.conf
echo "Configuring nsd.conf"
cp nsd.conf /etc/nsd/nsd.conf

#Restart and Enable NSD
echo "Starting NSD"
systemctl start nsd.service

echo "Enabling NSD"
systemctl enable nsd.service