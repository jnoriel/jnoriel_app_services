# READ ME

Introducing my first script project for Application Services! The process is simple for automatically deploying a *router* VM or a *mail server*. __Please Note__: that there are pre-requirements before running the following files for your VM scripts. The majority of the files are pre-configured to work with my current student number IP only and is mostly *hard-coded*:P. 

```
#12:	Noriel Jamaica	10.16.255.12/24 |10.16.12.0/24 | s12.as.learn
```

# **Pre-Requirement**

* Functional virtual machine(s) with an __intenet connection__. This is required to scp or transfer **PROJECT_FINAL** folder from your initial workstation to the VM(s) Inside of PROJECT_FINAL is also the **Mail_Setup** folder specifically for a mail server VM.

## **Setup for your Router or Primary VM**

*  Inside of __PROJECT_FINAL__ includes a **main.sh** which is a main invocation script that calls other scripts using main.conf for your router and/or your other VM's. You should check and use the **main.conf** file to set the boolean variable to *true* or *false* This default set up includes:

	1. base_configuration.sh: Base Packages, epel release, handy tools for your VM, VirtualBox Guest Additions and changing the hostname to: s12rtr.as.learn 

	2. selinux_setup.sh: Disables SELinux

	3. network_script.sh: Turns off/Disable Network Manager and FirewallD.

	4. net_if_setup.sh: Creates a pre-configured ifcfg-eth0 and ifcfg-eth1 file. 

	5. wifi_if_setup.sh: Install's hostapd (WiFi), configures a ifcfg-wlp0s6u2 __Note__: the WiFi interface "wlp0s6ux" name may change (x=number) And pre-configures.

	6. ospf_setup.sh: Install's and configure's routing table for OSPF (Quagga and Zebra).

	7. dhcp_setup.sh: Install's and configure's DHCP service.

	8. unbound_setup.sh: Configures recursive "DNS" name server.

	9. nsd_setup.sh: Configures authoritative "DNS" name server

	10. iptables_setup.sh: Pre-configures preliminary itpable setup and filter input/output rules. 

## **Setup for your Mail Server**

* Inside of __Mail_Setup__ has a series of scripts to create your mail server VM. First, start with using to run **mail_vm.sh** which calls out other scripts. Similar to the router/primary VM, instead, you use **main_mail.conf** to set the boolean variable to *true* or *false* This default setup includes a series of scripts including a pre-generated SSL .key/.crt for mail.s12.as.learn:  

	1. mail_base_configuration.sh: Install's base packages, epel release, handy tools for your VM, VirtualBox Gues Additions and changing the hostname to: mail.s12.as.learn

	2. selinux_setup.sh: Disables SELinux

	3. network_script.sh: Turns off/Disable Network Manager and FirewallD

	4. mail_if_setup.sh: Creates an ifcfg-eth0 file and configures the interface. 

	5. postfix_setup.sh: Configures a PostFix Mail server serivce with a pre-config file for *main.cf* and *master.cf*

	6. dovecot_setup.sh: Configures a Dovecot Mail server service with a pre-config file for *10-mail.conf*, *10-auth.conf*, *10-master.conf*. 

	