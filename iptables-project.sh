###################################################################################################
#Preliminary Setup
###################################################################################################
#Flush Tables
iptables -F -t filter
iptables -F -t nat

#Delete all user Defined Chains
iptables -X

###################################################################################################
#Filter Input Rules (i.e. for datagrams coming into the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P INPUT DROP

iptables -t filter -A INPUT -i lo -j ACCEPT

iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#SSH to router
iptables -t filter -A INPUT -i eth0 -d 10.16.255.12 -p tcp --dport 22 -m state --state NEW -j ACCEPT
#SSH to mail
iptables -t filter -A INPUT -i eth0 -d 10.16.12.1 -p tcp --dport 22 -m state --state NEW -j ACCEPT
#Allow OSPF
iptables -t filter -A INPUT -i eth0 -d 224.0.0.5 -p 89 -j ACCEPT

iptables -t filter -A INPUT -i eth0 -d 224.0.0.6 -p 89 -j ACCEPT
#DNS to router+mail+ping

iptables -t filter -A INPUT -i eth1 -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -t filter -A INPUT -i eth0 -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -t filter -A INPUT -i eth1 -p udp --dport 53 -j ACCEPT
iptables -t filter -A INPUT -i eth0 -p udp --dport 53 -j ACCEPT

#Allow ICMP
iptables -t filter -A INPUT  -d 10.16.255.12 -p icmp -j ACCEPT

#For Wireless Adapter

iptables -t filter -A INPUT -i wlp0s6u1 -p tcp --dport 53 -m state --state NEW -j ACCEPT

iptables -t filter -A INPUT -i wlp0s6u1 -p udp --dport 53 -j ACCEPT

#DHCP

iptables -t filter -A INPUT -i wlp0s6u1 -p udp --dport 67 -j ACCEPT

#Log table

iptables -t filter -A INPUT -j LOG


###################################################################################################
#Filter Output Rules (i.e. for datagrams leaving the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P OUTPUT ACCEPT

###################################################################################################
#Filter Forward Rules (i.e. for datagrams being forwarded by the router)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P FORWARD ACCEPT

iptables-save > /etc/sysconfig/iptables