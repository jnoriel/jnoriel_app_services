#! /bin/bash

#Creating ifcfg files for network-scripts

#Copy ifcfg-eth0 file to /etc/sysconfig/network-scripts
echo "Creating ifcfg-eth0 on /etc/sysconfig/network-scripts"
cp ifcfg-eth0 /etc/sysconfig/network-scripts

#Restart network
echo "Restarting network after creating and configuring ifcfg-eth0"
systemctl restart network.service
systemctl enable network.service