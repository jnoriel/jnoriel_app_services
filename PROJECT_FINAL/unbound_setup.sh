#! /bin/bash

#Set up unbound - configure recurisive name server

#Install unbound
echo "Installing unbound"
yum install -y unbound

echo "Downloading list of root name servers to for unbound"
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints

echo "Configuring unbound.conf"
cp ./unbound.conf /etc/unbound/unbound.conf

echo "Restarting and enabling unbound"
systemctl restart unbound
systemctl enable unbound