#!/bin/bash

#Gives parameters as a string
var1="$*" #one word
var2="$@" #seperate words
echo $var1
#Double quotes " treat the variable as a single string
for var in "$var1"; do
	echo "$var"
done
#Gives parameters as a list
echo $var2
#No quotes treat it as a list
for var in $var2; do
	echo "$var"
done

#Prints out parameters 0,1,2
#0 is the filename, everything after is a parameter
echo $0
echo $1
echo $2

echo $1$2$3
