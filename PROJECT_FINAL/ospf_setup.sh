#! /bin/bash

#Install OSPF/Quagga

echo "Installing quagga for OSPF"
yum install -y quagga

echo "Configuring zebra.conf"
cp zebra.conf /etc/quagga/zebra.conf

echo "Configuring ospfd.conf"
cp ospfd.conf /etc/quagga/ospfd.conf

echo "Changing ownership for /etc/quagga to the quagga user"
chown -R quagga:quagga /etc/quagga

echo "Starting and Enabling Zebra and Ospfd"
systemctl start ospfd.service zebra
systemctl enable ospfd.service zebra