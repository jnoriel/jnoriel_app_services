set -o nounset

#!/bin/bash

declare exit

FILE=$1    

if [ -e $FILE ]; then
   echo "File: $FILE exists."
exit=0

else
   echo "File: $FILE cannot be found."
exit=1

fi

exit $exit