#! /bin/bash

#Configure networking system and disable "FirewallD" and "Network Manager"


echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

echo "Turning off and disabling FirewallD"
systemctl stop firewalld.service
systemctl disable firewalld.service 

echo "Enabling and starting Network Service"
systemctl enable network.service
systemctl start network.service 