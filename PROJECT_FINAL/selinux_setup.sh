#! /bin/bash

#Configure and Disable SELinux 

echo "Disabling SELinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config