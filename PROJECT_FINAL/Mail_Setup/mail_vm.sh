#! /bin/bash

. ./mail_base_configuration.sh
. ./selinux_setup.sh
. ./network_script.sh
. ./mail_if_setup.sh

FILE=main_mail.conf
vars=()
declare -A options

while read LINE; do
        vars+=($LINE)
done < $FILE
for var in ${vars[@]}; do
        splitVars=(${var//=/ })
        options[${splitVars[0]}]=${splitVars[1]}
done


#To deploy and enable Postfix

if [[ ${options["postfix"]} == "true" ]]; then
	echo " Configuring PostFix Mail Server setup"
	. ./postfix_setup.sh
else 
	echo "Ignoring PostFix Mail Server setup"

fi

#Move the key/crt/csr
mv ./mail.s12.as.learn.crt /etc/pki/tls/certs
mv ./mail.s12.as.learn.key /etc/pki/tls/private
mv ./mail.s12.as.learn.0.csr /etc/pki/tls/certs

#To deploy and enable Dovecot

if [[ ${options["dovecot"]} == "true" ]]; then
	echo "Configuring Dovecot Mail Server setup"
	. ./dovecot_setup.sh
else 
	echo "Ignoring Dovecot Mail Server setup"

fi